package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.model.FrameShots;
import com.jobsity.challenge.model.FramesResult;
import com.jobsity.challenge.model.PlayerShots;
import com.jobsity.challenge.model.Shot;
import com.jobsity.challenge.service.IScoreCalculatorService;
import com.jobsity.challenge.service.IShotsGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ScoreCalculatorService implements IScoreCalculatorService {

    private static final int SHOTS_TO_JUMP_WHEN_NOT_STRIKE = 2;
    private static final Integer MAX_PINFALL = 10;
    private static final Integer TOTAL_FRAMES = 10;

    private int sum = 0;
    private int shotIndex = 0;

    private IShotsGeneratorService shotsGeneratorService;

    @Autowired
    public void setShotsGeneratorService(IShotsGeneratorService shotsGeneratorService) {
        this.shotsGeneratorService = shotsGeneratorService;
    }

    public FramesResult calculateScore(final PlayerShots playerShots) {

        final List<Integer> scores = new ArrayList<>();
        final List<String> shots = playerShots.getShots();
        final FrameShots frameShots = FrameShots.createFrameShot();
        int resultIndex = 1;
        sum = 0;
        shotIndex = 0;

        do {
            List<Shot> shotList = shotsGeneratorService.getShotSequenceForProcessing(shots, shotIndex);
            processFrame(frameShots, resultIndex, shotList);
            scores.add(sum);
            resultIndex++;

        } while (resultIndex <= TOTAL_FRAMES);

        return FramesResult.createFrameResult(scores, frameShots, playerShots.getPlayer());
    }

    private void processFrame(FrameShots frameShots, int resultIndex, List<Shot> shotList) {
        final Shot actualShot = shotList.get(0);
        if (resultIndex == TOTAL_FRAMES) {
            processLastFrame(shotList, frameShots);
        } else if (actualShot.isStrike()) {
            processStrike(shotList, frameShots);
        } else if (actualShot.isSpare(shotList.get(1))) {
            processSpare(shotList, frameShots);
        } else {
            processNormalFrame(shotList, frameShots);
        }
    }

    private void processLastFrame(List<Shot> shotList, FrameShots frameShots) {
        frameShots.addLastFrame(shotList);
        sum += shotList.stream().mapToInt(Shot::getValue).sum();
        shotIndex++;
    }

    private void processStrike(List<Shot> shotList, FrameShots frameShots) {
        frameShots.addStrikeFrame();
        sum += MAX_PINFALL + shotList.get(1).getValue() + shotList.get(2).getValue();
        shotIndex++;
    }

    private void processSpare(List<Shot> shotList, FrameShots frameShots) {
        frameShots.addSpareFrame(shotList.get(0));
        sum += MAX_PINFALL + shotList.get(2).getValue();
        shotIndex += SHOTS_TO_JUMP_WHEN_NOT_STRIKE;
    }

    private void processNormalFrame(List<Shot> shotList, FrameShots frameShots) {
        final List<Shot> normalFrameShots = Arrays.asList(shotList.get(0), shotList.get(1));
        frameShots.addNormalFrame(normalFrameShots);
        sum += normalFrameShots.stream().mapToInt(Shot::getValue).sum();
        shotIndex += SHOTS_TO_JUMP_WHEN_NOT_STRIKE;
    }

}
